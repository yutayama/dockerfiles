# -*- coding: utf-8; mode: makefile -*-

IMAGE_URL    ?= 
IMAGE_NAME   ?= riscv_tools
IMAGE_TAG    ?= latest

CHISEL3_VERSION = v3.5.4

DOCKER        = docker
DOCKERBUILD   = $(DOCKER) build
DOCKERIMAGE   = $(IMAGE_NAME):$(IMAGE_TAG)
ifneq ($(IMAGE_URL),)
DOCKERIMAGE   = $(IMAGE_URL)/$(DOCKERIMAGE)
endif
DOCKERRUN     = $(DOCKER) run \
	-it --rm \
	-v /etc/passwd:/etc/passwd:ro \
	-v /etc/group:/etc/group:ro \
	-v /etc/timezone:/etc/timezone:ro \
	-v $(HOME):$(HOME) \
	--net host \
	--name $DOCKER_CONTAINER \
	-u $(shell id -u):$(shell id -g) \
	-e "USER=$(USER)" \
	-e "HOME=$(HOME)" \
	-e "DISPLAY=$(DISPLAY)" \
	--workdir=$(shell pwd) \
	$(DOCKERIMAGE)

cpu_cores     = $(shell grep cpu.cores /proc/cpuinfo | sort -u | awk -F':' '{print $$2}' | sed -e 's/ //g')
MAX_JOBS     ?= $(cpu_cores)

info:
	@echo "MAX_JOBS : $(MAX_JOBS)"
	@echo "IMAGE_TAG: $(IMAGE_TAG)"
	@echo "HOME     : $(HOME)"
	@echo "USER     : $(USER)"
	@echo "DISPLAY  : $(DISPLAY)"

image:
	$(DOCKERBUILD) . -t $(DOCKERIMAGE) --build-arg MAX_JOBS=$(MAX_JOBS)

chisel:
	$(DOCKERRUN) git clone https://github.com/chipsalliance/chisel3.git -b ${CHISEL3_VERSION}

bash:
	$(DOCKERRUN) /bin/bash

subash:
	. ./activate $(DOCKERIMAGE) && docker_suexec /bin/bash
