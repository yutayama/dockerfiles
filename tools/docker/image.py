#!/usr/bin/env python
#-*- coding: utf-8 -*-

"""Docker Image class"""

import subprocess

from logging import getLogger
from pathlib import Path
from subprocess import PIPE

logger = getLogger(__name__)


class Image:
    """A class of Docker image"""

    @classmethod
    def path2name(cls, path):
        p = Path(path).resolve()
        tag = p.name
        name = p.parent.name
        url = p.parent.parent.name
        return (url, name, tag)

    def __init__(self, path, name=None, *, logger=logger, dry_run=False):
        self.path = Path(path).resolve()
        self.url, self.name, self.tag = self.path2name(path)
        self.logger = logger
        self.dry_run = dry_run


    def dry_run(self, en=True):
        self.dry_run = en


    def build(self, *, proxy=None):

        if self.path == None:
            raise FileNotFoundError("path is None")
        
        dockerfile = Path(self.path) / "Dockerfile"
        self.logger.debug("Dockerfile path:{}".format(str(dockerfile)))

        if not dockerfile.is_file():
            raise FileNotFoundError("Not found: {}".format(str(dockerfile)))

        command = ["docker", "build"]
        command += ["--network", "host"]
        command += ["-t", self._image()]

        if proxy:
            command += [" --build-arg", "http_proxy={}".format(proxy)]

        command += [str(self.path)]
        self._run(command, dry_run=self.dry_run)


    def push(self):

        command = ["docker", "push", self._image()]
        self._run(command, dry_run=self.dry_run)


    def release(self):

        if not self.exists():
            self.build()
       
        self.push()


    def exists(self):
        command = ["docker", "image", "ls", "-q", self._image()]
        result = subprocess.run(command, stdout=PIPE)
        return len(result.stdout) > 0


    def _run(self, command, *, dry_run=False):
        if dry_run:
            print(" ".join(command))
        else:
            logger.debug("run:{}".format(command))
            subprocess.run(command)


    def _image(self):
        return self.url + "/" + self.name + ":" + self.tag


if __name__ == "__main__":
    from logging import DEBUG
    from logging import Formatter
    from logging import StreamHandler

    fmt = Formatter('%(levelname)s - %(message)s')
    hdl = StreamHandler()
    hdl.setLevel(DEBUG)
    hdl.setFormatter(fmt)
    logger.setLevel(DEBUG)
    logger.addHandler(hdl)
    logger.propagate = False

    try:
        for path in ("local/opencv/4.5.1", "local/opencv/4.5.1-dnn", "local/none/latest"):
            img = Image(path, dry_run=True)
            print("{} exists?: ".format(path), img.exists())
            img.build()
            img.release()
    except Exception as e:
        logger.exception(e)

