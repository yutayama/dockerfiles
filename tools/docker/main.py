#!/usr/bin/env python
#-*- coding: utf-8 -*-

"""Docker Utility"""

import subprocess

from argparse import ArgumentParser
from logging import getLogger
from os import environ
from pathlib import Path

from image import Image
from container import Container

logger = getLogger(__name__)

def build(args):
    image = Image(args.path, name=args.tag, logger=logger, dry_run=args.dry_run)
    image.build()

def release(args):
    raise NotImplemented


def start(args):
    container = Container(image=args.image, logger=logger, dry_run=args.dry_run)
    container.start()


def stop(args):
    container = Container(name=args.name, logger=logger, dry_run=args.dry_run)
    container.stop()


def exec(args):
    container = Container(name=args.name, logger=logger, dry_run=args.dry_run)
    container.exec(args.command)


def set_debug(level=0):
    from logging import DEBUG
    from logging import Formatter
    from logging import StreamHandler

    fmt = Formatter('%(levelname)s - %(message)s')
    # fmt = Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    # fmt = Formatter('%(asctime)s:%(levelname)s:%(name)s:%(funcName)s:%(lineno)s:%(message)s')
    # fmt = Formatter('%(asctime)s:%(levelname)s:%(funcName)s:%(lineno)s:%(message)s')
    hdl = StreamHandler()
    hdl.setLevel(DEBUG)
    hdl.setFormatter(fmt)
    logger.setLevel(DEBUG)
    logger.addHandler(hdl)
    logger.propagate = False
    logger.warning("debug mode (level={})".format(level))


def main():
    #usage = "Usage: python {} [OPTIONS] command [COMMAND_ARGS]".format(__file__)
    #parser = ArgumentParser(usage=usage)
    parser = ArgumentParser()

    # global options
    parser.add_argument('-v', '--verbose', action='store_true', help='verbose')
    parser.add_argument('-d', '--debug', action='store_true', help='DEBUG mode')
    parser.add_argument('--debug-level', type=int, default=1, help='DEBUG level')
    parser.add_argument('--dry-run', action='store_true', help='perform a trial run with no changes made')

    # sub command: build
    subparsers = parser.add_subparsers(help='', dest='command', required=True, metavar='subcommand')
    #subparsers = parser.add_subparsers(help='command')
    parser_build = subparsers.add_parser('build', help='build docker image')
    parser_build.set_defaults(handler=build)
    parser_build.add_argument('-t', '--tag', type=str, help='overwrite image tag')
    parser_build.add_argument('path', help='directory in the target Dockerfile')

    # sub command: release
    parser_release = subparsers.add_parser('release', help='release docker image')
    parser_release.set_defaults(handler=release)
    parser_release.add_argument('image', help='image tag')

    # sub command: start
    parser_start = subparsers.add_parser('start', help='start docker container')
    parser_start.set_defaults(handler=start)
    parser_start.add_argument('image', help='image tag')
    parser_start.add_argument('--container', type=str, help='container name')

    # sub command: stop
    parser_start = subparsers.add_parser('stop', help='stop docker container')
    parser_start.set_defaults(handler=stop)
    parser_start.add_argument('container', help='container name')

    # sub command: exec
    parser_start = subparsers.add_parser('exec', help='exec command in docker container')
    parser_start.set_defaults(handler=stop)
    parser_start.add_argument('container', help='container name')
    parser_start.add_argument('command', nargs='+', help='command')

    args = parser.parse_args()
    print(args)

    if args.debug:
        set_debug(args.debug_level)

    eval(args.command)(args)

    return

if __name__ == "__main__":
    main()
