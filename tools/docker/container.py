#!/usr/bin/env python
#-*- coding: utf-8 -*-

"""Docker Container class"""

import os
import subprocess

from logging import getLogger
from subprocess import PIPE

from image import Image

logger = getLogger(__name__)


class Container:
    """A class of Docker container"""

    def __init__(self, path, name=None, *, logger=logger, dry_run=False):
        self.image = Image(path, logger=logger, dry_run=dry_run)
        self.name = name or self.image._image()

        self.logger = logger
        self.dry_run = dry_run
        self.privileged = False


    def dry_run(self, en=True):
        self.dry_run = en


    def start(self):
        if self.image and not self.image.exists():
            self.image.build()

        command = ["docker", "run"]
        command += ["-itd", "--rm"]
        if self.privileged:
            command += ["--privileged"]
        command += ["-v", "/etc/passwd:/etc/passwd:ro"]
        command += ["-v", "/etc/group:/etc/group:ro"]
        command += ["-v", "/etc/timezone:/etc/timezone:ro"]
        command += ["-v", "{0}:{0}".format(os.environ.get("HOME"))]
        command += ["--net", "host"]
        command += ["--name", self.name]
        command += [self.image._image()]

        self._run(command, dry_run=self.dry_run)


    def exec(self, *args):
        print(type(args))
        print(args)
        self.logger.debug("args::".format(str(args)))

        if not self.exists():
            self.start()

        uid = os.getuid()
        gid = os.getgid() 
        pwd = os.getcwd()       

        command = ["docker", "exec"]
        command += ["-it"]
        command += ["-u {}:{}".format(str(uid), str(gid))]
        command += ["-e", "\"USER={}\"".format(os.environ.get("USER"))]
        command += ["-e", "\"HOME={}\"".format(os.environ.get("HOME"))]
        command += ["-e", "\"DISPLAY={}\"".format(os.environ.get("DISPLAY", ""))]
        command += ["--workdir={}".format(pwd)]
        command += [self.name]
        if args:
            command += args
        
        self._run(command, dry_run=self.dry_run)

    def exists(self):
        command = ["docker", "container", "ls", "-q", "-f", "name={}".format(self.name)]
        result = subprocess.run(command, stdout=PIPE)
        self.logger.debug(result)
        return len(result.stdout) > 0


    def _run(self, command, *, dry_run=False):
        if dry_run:
            print(" ".join(command))
        else:
            logger.debug("run:{}".format(command))
            subprocess.run(command)



if __name__ == "__main__":
    from logging import DEBUG
    from logging import Formatter
    from logging import StreamHandler

    fmt = Formatter('%(levelname)s - %(message)s')
    hdl = StreamHandler()
    hdl.setLevel(DEBUG)
    hdl.setFormatter(fmt)
    logger.setLevel(DEBUG)
    logger.addHandler(hdl)
    logger.propagate = False

    cnt_lists = (("yutayama_opencv_4.5.1", "local/opencv/4.5.1"),
                 ("yutayama_opencv_4.5.1-dnn", "local/opencv/4.5.1-dnn"),
                 ("yutayama_none_latest", "local/none/latest"))

    try:
        for c, path in cnt_lists:
            cnt = Container(c, path, dry_run=True)
            print("{} exists?: ".format(c), cnt.exists())
            cnt.start()
            cnt.exec("ls", "-al")
    except Exception as e:
        logger.exception(e)

    #gitlab = Container("gitlab-runner", "gitlab/gitlab-runner")
    #github = Container("github-runner", "gitlab/gitlab-runner")
    #print("gitlab exists?: ", gitlab.exists())
    #print("github exists?: ", github.exists())

